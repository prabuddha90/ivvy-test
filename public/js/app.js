$('#start_date').datepicker({
    format: 'yyyy-mm-dd',
    "setDate": new Date(),
    startDate: '+1d'
})
$('#end_date').datepicker({
    format: 'yyyy-mm-dd',
    "setDate": new Date(),
    startDate: '+1d'
})