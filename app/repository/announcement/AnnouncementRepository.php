<?php

namespace App\repository\announcement;


use App\Models\Announcement;

class AnnouncementRepository
{
    /**
     * summary
     */
    public function create($announcement)
    {
    	$anounce = new Announcement();
    	$anounce->title=$announcement->title;
    	$anounce->content=$announcement->content;
    	$anounce->start_date=$announcement->start_date;
    	$anounce->end_date=$announcement->end_date;
    	$anounce->active=1;
       	$anounce->save();
    	return $anounce;
        
    }

    public function update($announcementData, $id){
        $anounce = Announcement::find($id);        
        $anounce->title=$announcementData->title;
        $anounce->content=$announcementData->content;
        $anounce->start_date=$announcementData->start_date;
        $anounce->end_date=$announcementData->end_date;
        $anounce->active=1;              
        $anounce->save();
        return $anounce;

    }
}