<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Announcement;
use Carbon\Carbon;
class AnnouncementCountroller extends Controller
{
    public function index(){
        $announcements = Announcement::orderBy('start_date','ASC')->whereDate('end_date', '>=' , Carbon::now())->get();
        return response([ 'announcements' => $announcements]);
    }
}
